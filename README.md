# nomad-ci

# delpoy job definitions from the list

Include in your project in order to run a list of nomad jobs:

Job definitions yaml file must be named: 

*  **job-definitions-dev.yml** for dev jobs 
*  **job-definitions-prod.yml** for production jobs.

```
---

variables:
  NOMAD_ADDR_DEV: "${NOMAD_ADDR_DEV}"
  NOMAD_TOKEN_DEV: "${NOMAD_TOKEN_DEV}"
  SINGLE_JOB: "False"

include:
  - 'https://gitlab.cern.ch/db/nomad-ci/-/raw/master/nomad-ci.yml'

```

### Adding custom template:

Add the template name as set in job definitions yaml file.

Example:

```
---

variables:
  NOMAD_ADDR_DEV: "${NOMAD_ADDR_DEV}"
  NOMAD_TOKEN_DEV: "${NOMAD_TOKEN_DEV}"
  SINGLE_JOB: "False"
  TEMPLATE_NAME: "volume_copy.hcl"

include:
  - 'https://gitlab.cern.ch/db/nomad-ci/-/raw/master/nomad-ci.yml'
```

# deploy_single_job-ci

Include in your project in order to run it as a Nomad single job

Example of using its variables:

*  **TEMPLATE_NAME_DEV** - template ready to be rendered into final job specification
*  **JOB_NAME_DEV** - `must` be the same as name of the job set in the template
```
---

variables:
  NOMAD_ADDR_DEV: "${NOMAD_ADDR_DEV}"
  NOMAD_TOKEN_DEV: "${NOMAD_TOKEN_DEV}"
  TEMPLATE_NAME_DEV: "template_to_be_rendered.hcl"
  JOB_NAME_DEV: "example"

include:
  - 'https://gitlab.cern.ch/db/nomad-ci/-/raw/master/nomad-ci.yml'
```

# deploy job to multiple clusters

In order to deploy single job to other clusters at the same time, one has to set environment variables in .gitlab-ci.yml, specific for each cluster. 
These variables are **NOMAD_TOKEN_(XX)**, **TEMPLATE_NAME_(XX)**, **JOB_NAME_(XX)** where XX is the name of the cluster (DEV, PROD, AIS, DBOD...).


Example:

```
variables:
  NOMAD_TOKEN_DEV: "${NOMAD_TOKEN_DEV}"
  NOMAD_TOKEN_AIS: "${NOMAD_TOKEN_AIS}"
  NOMAD_TOKEN_DBOD: "${NOMAD_TOKEN_DBOD}"
  NOMAD_TOKEN_PROD: "${NOMAD_TOKEN_PROD}"
  TEMPLATE_NAME_AIS: "monitoring-ais.hcl"
  TEMPLATE_NAME_DBOD: "monitoring-dbod.hcl"
  TEMPLATE_NAME_DEV: "monitoring-dev.hcl"
  TEMPLATE_NAME_PROD: "monitong-prod.hcl"
  JOB_NAME_DEV: "monitoring-dev"
  JOB_NAME_AIS: 'monitoring-ais'
  JOB_NAME_DBOD: 'monitoring_dbod'
  JOB_NAME_PROD: 'monitoring_prod'

stages:
  - deploy

include:
  - 'https://gitlab.cern.ch/db/nomad-ci/-/raw/master/nomad-ci.yml'
```
